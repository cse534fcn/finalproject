import plotly.plotly as py
import plotly.figure_factory as ff
import plotly as pl
import plotly.graph_objs as go
import plotly.plotly as py
import plotly.graph_objs as go
import csv

def readfile(filename):
    parsed_file=[]
    start_times=[]
    end_times=[]
    is_ads=[]
    res=[]
    f = open(filename)
    for l in f.readlines():
        parsed_file.append(l.strip().split("\t"))
        #print l.strip().split("\t")
        sep = l.strip().split("\t")
        #print sep
        start_times.append(parsed_file[0])

    count=1
    list_dict=[]
    for i in range(len(parsed_file)):
        dict={}
        start_times.append(parsed_file[i][0])
        dict["Task"]=str(count)
        count=count+1
        dict["Start"]=str(parsed_file[i][0])
        dict["Finish"]=str(parsed_file[i][1])
        end_times.append(parsed_file[i][1])
        is_ads.append(parsed_file[i][2])
        r=""
        if parsed_file[i][2]=='true':
            res.append('Ad')
            r="Ad"
        else:
            res.append('Not ad')
            r="Not Ad"

        dict["Resource"]=str(r)

        list_dict.append(dict)

    df=list_dict
    pl.tools.set_credentials_file(username='gayatr', api_key='KLDeK71lb3FiOU7eH8h7')
    colors = {'Ad': 'rgb(220, 0, 0)',
              'Not Ad': 'rgb(0, 255, 255)',

              }
    fig = ff.create_gantt(df,colors=colors, index_col='Resource', show_colorbar=True, group_tasks=True,bar_width=0.8,showgrid_x=True, showgrid_y=True,title='Critical Path')
    py.plot(fig, filename='gantt-numeric-variable', world_readable=True)


def main():
    readfile("C:\\Users\\Gayatri\\Desktop\\fcnproject\\finalproject\\src\\critical path\\CriticalPath_diply.har.txt")



def represent(filename):
    harfiles=[]
    totaltimes=[]
    adtimes=[]
    totalcontent=[]
    adcontent=[]
    with open(filename, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in reader:
            harfiles.append(row[0])
            totaltimes.append(row[3])
            adtimes.append(row[4])
            totalcontent.append(row[1])
            adcontent.append(row[2])

    trace1 = go.Bar(
        x=harfiles,
        y=totaltimes,
        name='Total Time'
    )
    trace2 = go.Bar(
        x=harfiles,
        y=adtimes,
        name='Ad Time'
    )

    trace3 = go.Bar(
        x=harfiles,
        y=totalcontent,
        name='Total Page content'
    )

    trace4 = go.Bar(
        x=harfiles,
        y=adcontent,
        name='Ad Content'
    )

    data = [trace1, trace2]
    layout = go.Layout(
        barmode='stack'
    )

    fig = go.Figure(data=data, layout=layout)
    py.plot(fig, filename='Time Ratio')

    data = [trace3, trace4]
    layout = go.Layout(
        barmode='stack'
    )

    fig = go.Figure(data=data, layout=layout)
    py.plot(fig, filename='Size ratio')


def criticalPath(filename):
    harfiles = []
    criticalPathSize=[]
    adCount = []
    with open(filename, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in reader:
            harfiles.append(row[0])
            criticalPathSize.append(row[7])
            adCount.append(row[8])

    trace1 = go.Bar(
        x=harfiles,
        y=criticalPathSize,
        name='Critical Path Size'
    )
    trace2 = go.Bar(
        x=harfiles,
        y=adCount,
        name='Ad Count'
    )

    data = [trace1, trace2]
    layout = go.Layout(
        barmode='group'
    )

    fig = go.Figure(data=data, layout=layout)
    py.plot(fig, filename='Ad on Critical Path')

def tablechart(filename):
    rowlist1=[]
    rowlist2 = []
    rowlist3 = []
    rowlist4 = []
    i=0
    with open(filename, 'rb') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in reader:
            if i==0:
                rowlist1.append(row[0])
                rowlist1.append(row[1])
                rowlist1.append(row[2])
                rowlist1.append(row[3])
                rowlist1.append(row[4])
                rowlist1.append(row[5])
                rowlist1.append(row[6])
                i=i+1
            elif i==1:
                rowlist2.append(row[0])
                rowlist2.append(row[1])
                rowlist2.append(row[2])
                rowlist2.append(row[3])
                rowlist2.append(row[4])
                rowlist2.append(row[5])
                rowlist2.append(row[6])
                i = i + 1
            elif i==2:
                rowlist3.append(row[0])
                rowlist3.append(row[1])
                rowlist3.append(row[2])
                rowlist3.append(row[3])
                rowlist3.append(row[4])
                rowlist3.append(row[5])
                rowlist3.append(row[6])
                #rowlist3.append(row[0] + ',' + row[1] + ',' + row[2] + ',' + row[3] + ',' + row[4] + ',' + row[5] +
                # ',' + row[6])
                i = i + 1
            else:
                rowlist4.append(row[0])
                rowlist4.append(row[1])
                rowlist4.append(row[2])
                rowlist4.append(row[3])
                rowlist4.append(row[4])
                rowlist4.append(row[5])
                rowlist4.append(row[6])




    print rowlist1
    data_matrix = [['Har File', 'Content Size', 'Ad Size', 'Total Content Time', 'Ad Time','Ad Size %','Ad Time %'],
                   rowlist1,
                   rowlist2,
                   rowlist3,
                   rowlist4]

    table = ff.create_table(data_matrix)
    py.plot(table, filename='latex_table')

main()
represent("C:\\Users\\Gayatri\\Desktop\\fcnproject\\finalproject\\src\\Analysis_PLT_size_all.csv")
criticalPath("C:\\Users\\Gayatri\\Desktop\\fcnproject\\finalproject\\src\\Analysis_PLT_size_all.csv")
tablechart("C:\\Users\\Gayatri\\Desktop\\fcnproject\\finalproject\\src\\Analysis_PLT_size_all.csv")