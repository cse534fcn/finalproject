import java.io.File;
import java.util.Comparator;
import java.util.*;
import java.io.IOException;
import java.util.List;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import org.joda.time.DateTime;
import org.codehaus.jackson.JsonParseException;

import edu.umass.cs.benchlab.har.*;
import edu.umass.cs.benchlab.har.tools.*;

class CriticalPath{
	public List<HarEntry> nodeC;
	public List<Boolean> isAdC;
	public List<DateTime> endTimeC;
}

class HarComparator implements Comparator<HarEntry>
{
    public int compare(HarEntry c1, HarEntry c2)
    {
        return c1.getStartedDateTime().compareTo(c2.getStartedDateTime());
    }
}

public class ReadHARS {
	void readFile(String fileName, List<String> adURL) throws IOException {
		Scanner s = new Scanner(new File(fileName));
		
		while (s.hasNext()){
		    adURL.add(s.next());
		}
		s.close();
	}

//	DateTime getMyStartedDateTime(HarEntry entry){
//		String temp = entry.toString();
//		System.out.println(temp);
//		String time = "";
//		System.out.println(temp.lastIndexOf("2017-04-25T"));
//		int i = temp.lastIndexOf("2017-04-25T");
//		while(temp.charAt(i) != '\"'){
//			time += temp.charAt(i);
//			i++;
//			System.out.println("Atime: "+time);
//		}
//		DateTime obj = new DateTime(time);
//		System.out.println("time: "+obj);
//		return obj;
//	}

	public static void main(String[] args) throws IOException {
		String[] listOfFiles = {"cnn.har","tmz.har","ebay.har","ntd.har"};
		//String[] listOfFiles = {"alibaba.har","cnn.har","nytimes.har"};
		//String[] listOfFiles = {"tmz.har"};
		
//			{"1google.har","2youtube.har",//"3facebook.har",
//								"4baidu.har","360.har",
//								"amazon.har","google.co.in.har","google.co.jp.har","google.co.uk.har",
//								"google.de.har","instagram.har","jd.har","linkedin.har","live.har",
//								"qq.har","reddit.har","sina.com.cn.har","sohu.har","taobao.har",
//								"tmall.har",,"vk.har","weibo.har",,"yahoo.har"};
//		
		String fileForPLT = "Analysis_PLT_size.csv";
		BufferedWriter outfile = new BufferedWriter(new FileWriter(fileForPLT));
		for(String filename : listOfFiles){
			File f = new File(filename);
		    HarFileReader r = new HarFileReader();
		    HarFileWriter w = new HarFileWriter();
		    List<String> adURL = new ArrayList<String>() ;
		    ReadHARS obj = new ReadHARS();
		    obj.readFile("ads.txt", adURL);
		    long totalAdSize=0, totalEntrySize=0, totalTime=0, totalAdTime=0, adTime=0, entryTime=0;
		    double adSizePerc,adTimePerc;
		    int adOnCriticalPath = 0;
		    List<DateTime> adEndTime = new ArrayList<DateTime>();
		    List<DateTime> adStartTime = new ArrayList<DateTime>();
		    List<DateTime> endTime = new ArrayList<DateTime>();
		    List<DateTime> startTime = new ArrayList<DateTime>();
		    List<Boolean> isAd ;
		    List<CriticalPath> criticalPath = new ArrayList<CriticalPath>();
		    
		    try
		    {
		      System.out.println("Reading " + filename);
		      // All violations of the specification generate warnings
		      List<HarWarning> warnings = new ArrayList<HarWarning>();
		      HarLog log = r.readHarFile(f, warnings);
		      
		      // Access all elements as objects
		      HarEntries entries = log.getEntries();
		      List<HarEntry> hentry = entries.getEntries(); 
		      
		      Collections.sort(hentry, new HarComparator());

		      isAd = new ArrayList<Boolean>();
	    	  for(int i=0;i<hentry.size();++i)
	    	  {
	    		  isAd.add(false);
	    	  }
	    	  
		      //Output "response" code of entries.
	    	  int i=-1;
		      for (HarEntry entry : hentry)
		      {
		    	  i++;
		    	  
		    	  
		    	  for(String URL : adURL)
		          {
		        	  //AD present
		        	  if(entry.getRequest().toString().toLowerCase().contains(URL)){
		        		  isAd.set(i,true);
		        		  
		        		  totalAdSize += entry.getResponse().getHeadersSize() + entry.getResponse().getBodySize();
		        		  
		        		  //adStartTime.add(obj.getMyStartedDateTime(entry));
		        		  adStartTime.add(new DateTime(entry.getStartedDateTime()));
		        		  //System.out.println("Start time2: "+adStartTime.get(adStartTime.size()-1));
		        		  adEndTime.add(new DateTime (adStartTime.get(adStartTime.size()-1).plusMillis((int)entry.getTime())));
		        		  adTime += entry.getTime();
		        		  
		        		  if(entry.getTimings().getBlocked() > -1)
		        			  totalAdTime += entry.getTimings().getBlocked();
		        		  if(entry.getTimings().getDns() > -1)
		        			  totalAdTime += entry.getTimings().getDns();
		        		  if(entry.getTimings().getConnect() > -1)
		        			  totalAdTime += entry.getTimings().getConnect();
		        		  if(entry.getTimings().getSend() > -1)
		        			  totalAdTime += entry.getTimings().getSend();
		        		  if(entry.getTimings().getWait() > -1)
		        			  totalAdTime += entry.getTimings().getWait();
		        		  if(entry.getTimings().getReceive() > -1)
		        			  totalAdTime += entry.getTimings().getReceive();
		        	  }
		          }
		          //startTime.add(obj.getMyStartedDateTime(entry));
		          startTime.add(new DateTime(entry.getStartedDateTime()));
	    		  
		          endTime.add((startTime.get(startTime.size()-1).plusMillis((int)entry.getTime())));
		          entryTime += entry.getTime();
		          if(entry.getTimings().getBlocked() > -1)
	    			  totalTime += entry.getTimings().getBlocked();
	    		  if(entry.getTimings().getDns() > -1)
	    			  totalTime += entry.getTimings().getDns();
	    		  if(entry.getTimings().getConnect() > -1)
	    			  totalTime += entry.getTimings().getConnect();
	    		  if(entry.getTimings().getSend() > -1)
	    			  totalTime += entry.getTimings().getSend();
	    		  if(entry.getTimings().getWait() > -1)
	    			  totalTime += entry.getTimings().getWait();
	    		  if(entry.getTimings().getReceive() > -1)
	    			  totalTime += entry.getTimings().getReceive();
	        	  totalEntrySize += entry.getResponse().getHeadersSize() + entry.getResponse().getBodySize();
		      }

		      System.out.println("Total size is:"+totalEntrySize);
		      System.out.println("Total Ad size is:"+totalAdSize);
		      System.out.println("Total time is:"+totalTime);
		      System.out.println("Total Ad time is:"+totalAdTime);
		      System.out.println("Entry Time is:"+entryTime);
		      System.out.println("Ad time is:"+adTime);
		      adSizePerc = (float)totalAdSize*100/(float)totalEntrySize;
		      adTimePerc = (float)totalAdTime*100/(float)totalTime;
		      System.out.println("Ad %:" + adSizePerc);
		      System.out.println("Ad time %:" + adTimePerc);
		      
//		      for(Boolean ifAd: isAd ){
//		    	  System.out.println(ifAd);
//		      }
		      outfile.write(String.valueOf(totalEntrySize));
		      outfile.write(",");
		      outfile.write(String.valueOf(totalAdSize));
		      outfile.write(",");
		      outfile.write(String.valueOf(totalTime));
		      outfile.write(",");
		      outfile.write(String.valueOf(totalAdTime));
		      outfile.write(",");
		      outfile.write(String.valueOf(adSizePerc));
		      outfile.write(",");
		      outfile.write(String.valueOf(adTimePerc));
		      
		      
		      
		      List<HarEntry> tempHar = new ArrayList<HarEntry>();
		      List<DateTime> tempEndTime = new ArrayList<DateTime>();
		      List<Boolean> tempAd = new ArrayList<Boolean>();
	    	  CriticalPath temp = new CriticalPath();
	    	  tempHar.add(hentry.get(0));
	    	  tempAd.add(isAd.get(0));
	    	  tempEndTime.add(endTime.get(0));
	    	  temp.nodeC = tempHar;
	    	  temp.isAdC = tempAd;
	    	  temp.endTimeC = tempEndTime;
	    	  criticalPath.add(temp);
	    	  
	    	  int j=1;
	    	  for (HarEntry harEntry : hentry){
	    		  
	    		  if(harEntry == hentry.get(0))
	    			  continue;
	    		  DateTime prevEnd =criticalPath.get(criticalPath.size()-1).endTimeC.get(criticalPath.get(criticalPath.size()-1).endTimeC.size()-1);
		    	  DateTime currentStart = new DateTime (harEntry.getStartedDateTime());
		    	  DateTime currentEnd = new DateTime(endTime.get(j));
		    	  
		    	  //Add new critical path object if prev endtime < current start time
		    	  if(prevEnd.isBefore(currentStart) || currentEnd.isAfter(prevEnd)){
		    		  //System.out.println("hentry:"+harEntry);
		    		  List<HarEntry> tempHar1 = new ArrayList<HarEntry>();
		    	      List<DateTime> tempEndTime1 = new ArrayList<DateTime>();
		    	      List<Boolean> tempAd1 = new ArrayList<Boolean>();
		        	  CriticalPath temp1 = new CriticalPath();
		        	  tempHar1.add(harEntry);
		        	  tempAd1.add(isAd.get(j));
		        	  tempEndTime1.add(endTime.get(j));
		        	  temp1.nodeC = tempHar1;
		        	  temp1.isAdC = tempAd1;
		        	  temp1.endTimeC = tempEndTime1;
		        	  criticalPath.add(temp1);
		    	  }
		    	  //if current end time is less than prev end time, merge the current har entry into previous one
		    	  else if (currentEnd.isBefore(prevEnd)){
		    		  criticalPath.get(criticalPath.size()-1).nodeC.set(criticalPath.get(criticalPath.size()-1).nodeC.size()-1,harEntry);
		    		  criticalPath.get(criticalPath.size()-1).isAdC.set(criticalPath.get(criticalPath.size()-1).isAdC.size()-1,isAd.get(j));
		    		  criticalPath.get(criticalPath.size()-1).endTimeC.set(criticalPath.get(criticalPath.size()-1).endTimeC.size()-1,currentEnd);
		    	  }
		    	  //the entry start before prevEnd but ends after current end
//		    	  else{
//		    			  criticalPath.get(criticalPath.size()-1).endTimeC.set(criticalPath.get(criticalPath.size()-1).endTimeC.size()-1,prevEnd);
//		    		  }
//		    	
		    	  j++;
		       }
		       
	    	  /*Print Critical Path*/

	  	    //Write start time and end time of critical path to the file
	    	  BufferedWriter out = new BufferedWriter(new FileWriter("CriticalPath_"+filename+".txt"));
	    	  
	  	       
	    	  for(CriticalPath path: criticalPath){
	    		  int k=0;
	    		  for(HarEntry har: path.nodeC){
	    			  //DateTime startedTime = new DateTime(har.getStartedDateTime());
	    			  //out.write(startedTime.getMillisofSecond());
	    			  out.write(new DateTime(har.getStartedDateTime()).toString());
	    			  out.write("\t");
	    			 // out.write((int)path.endTimeC.get(k).getMillisOfSecond());
	    			  //out.write(path.endTimeC.get(k).toString());
	    			  out.write(path.endTimeC.get(k).toString());
	    			  out.write("\t");
	    			  out.write(path.isAdC.get(k).toString());
	    			  k++;
	    			  out.newLine();
	    		  }
	    		  if(path.isAdC.size()==1 && path.isAdC.get(0))
	    			  adOnCriticalPath++;
	    	  }

	    	  
	    	  out.close();
	    	  System.out.println("CP size:"+criticalPath.size());
	    	  outfile.write(",");
	    	  outfile.write(String.valueOf(criticalPath.size()));
		      System.out.println("number of ad on CP:"+adOnCriticalPath);
		      outfile.write(",");
		      outfile.write(String.valueOf(adOnCriticalPath));
		      //%of ad on critical path
		      outfile.write(",");
		      outfile.write(String.valueOf((float)adOnCriticalPath*(100)/(float)criticalPath.size()));

		      outfile.newLine();
		    }
		    catch (JsonParseException e)
		    {
		      e.printStackTrace();
		      //fail("Parsing error during test");
		    }
		    catch (IOException e)
		    {
		      e.printStackTrace();
		      //fail("IO exception during test");
		    }
		}
	    
	  outfile.close();  
}
}
